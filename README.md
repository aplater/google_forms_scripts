# google_forms_scripts

Script was used to import from google sheets into google forms as responses. Form needs to not have email collection enabled because it will prevent the response from being submitted. There may be some uncovered field types but you can implement them inside the switch statement by following the established pattern. See https://developers.google.com/apps-script/reference/forms/item for types.

samglockner@pm.me
